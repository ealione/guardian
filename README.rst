GUARDIAN
=================================

|Join the chat at https://gitter.im/guardian-python/Lobby| |Build Status| |PyPI| |PyPI version|

describe the main idea here

KIOT can be found `on GitHub <https://github.com/>`_.


Benchmarks
----------

All tests were run using ....

+-----------+-----------------------+----------------+---------------+
| Server    | Implementation        | Requests/sec   | Avg Latency   |
+===========+=======================+================+===============+
| Serv 1    | Python 3.5 + RPis     | 33,342         | 2.96ms        |
+-----------+-----------------------+----------------+---------------+


Hello World Example
-------------------

.. code:: python

    from guardian import Guardian

    app = Guardian()


Installation
------------

-  ``python -m pip install guardian``

To install guardian from source ...

- ``git clone https://github.com/``
- ``cd GOUARDIAN``
- ``pip install -r requirements.txt``
- ``python setup install``


Installation
------------

Unit tests can be run by using the following command

- ``python -m unittest discover -s <directory> -p '*_test.py'``


Running
-------

You can run KIOT using

- ``python guardian root -c Config/config_root_1.yaml -vvv``
- ``python guardian broker -c Config/config_broker_1.yaml -vvv``
- ``python guardian leaf -c Config/config_leaf_1.yaml -vvv``
- ``python guardian client -c Config/config_client_1.yaml -vvv``


Documentation
-------------

`Documentation on Readthedocs <http://>`_.

.. |Join the chat at https://gitter.im/guardian-python/Lobby| image:: https://badges.gitter.im/guardian-python/Lobby.svg
   :target: https://gitter.im/guardian-python/Lobby?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge
.. |Build Status| image:: https://travis-ci.org/channelcat/guardian.svg?branch=master
   :target: https://travis-ci.org/channelcat/guardian
.. |Documentation| image:: https://readthedocs.org/projects/guardian/badge/?version=latest
   :target: http://sanic.readthedocs.io/en/latest/?badge=latest
.. |PyPI| image:: https://img.shields.io/pypi/v/guardian.svg
   :target: https://pypi.python.org/pypi/guardian/
.. |PyPI version| image:: https://img.shields.io/pypi/pyversions/guardian.svg
   :target: https://pypi.python.org/pypi/guardian/


TODO
----
* something yet to be decided


Limitations
-----------
* probably a lot


THIS README IS SIMPLY A PLACEHOLDER, do not trust anything it claims... or does
