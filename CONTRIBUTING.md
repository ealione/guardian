## Coding Style
We use isort and black to format our code, you can use the following commands to format your code prior to submission:

```
(pytext_venv) $ pip install isort black
(pytext_venv) $ black guardian
(pytext_venv) $ isort guardian --recursive --multi-line 3 --trailing-comma --force-grid-wrap 0 --line-width 88 --lines-after-imports 2 --combine-as --section-default THIRDPARTY
```
