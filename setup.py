# -*- coding: utf-8 -*-
from __future__ import with_statement
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

__license__ = """
    This file is part of GUARDIAN.

    GUARDIAN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GUARDIAN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GUARDIAN.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Esmerald Aliaj'
__email__ = ''

options = {}

with open('README.rst') as fp:
    README = fp.read().strip() + "\n\n"

ChangeLog = (
    "What's new\n"
    "==========\n"
    "\n")
with open('CHANGELOG.md') as fp:
    ChangeLog += fp.read().strip()

LONG_DESCRIPTION = README + ChangeLog
CLASSIFIERS = [
    'Development Status :: 5 - Production/Stable',
    'Intended Audience :: Developers',
    'Intended Audience :: System Administrators',
    'Intended Audience :: Science/Research',
    'Environment :: Console',
    'Environment :: Plugins',
    'Topic :: Software Development :: Libraries :: Python Modules',
    'Topic :: Communications',
    'Topic :: Internet',
    'Topic :: System :: Networking',
    'License :: OSI Approved :: BSD License',
    'Operating System :: OS Independent',
    'Operating System :: MacOS :: MacOS X',
    'Operating System :: POSIX :: Linux',
    'Natural Language :: English',
    'Programming Language :: Python',
    'Programming Language :: Python :: 3',
]

exec(open('guardian/_version.py').read())

setup(
    name='GUARDIAN',
    version=__version__,
    packages=['guardian'],
    url='https://github.com/',
    download_url='https://github.com/',
    license='LGPLv3',
    author='',
    install_requires=['msgpack-python', 'zmq', 'pyyaml'],
    author_email='',
    maintainer="",
    maintainer_email="",
    description='',
    long_description=LONG_DESCRIPTION,
    zip_safe=False,
    use_2to3=True,
    keywords="security statistics iot networks",
    classifiers=CLASSIFIERS,
    entry_points={"console_scripts": ["guardian = guardian.main:main"]},
)
