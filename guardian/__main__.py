#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import yaml
import zmq
from argparse import ArgumentParser
from logging import config, basicConfig, captureWarnings, getLogger, CRITICAL, ERROR, WARNING, INFO, DEBUG, \
    StreamHandler
from logging.handlers import TimedRotatingFileHandler, RotatingFileHandler
from zmq.log.handlers import PUBHandler

from g_broker_runner import run as br
from g_worker_runner import run as wr
from g_client_runner import run as cr
from _version import __version__


# TODO: review this, does the idea make sense
# TODO: blocks messages if started in multiple processes
def network_handler(protocol='tcp', endpoint='*', port='4547'):
    ctx = zmq.Context()
    pub = ctx.socket(zmq.PUB)
    try:
        pub.bind('%s://%s:%s' % (protocol, endpoint, port))
    except zmq.error.ZMQError:
        print("Logger::Network logger endpoint is already in use!")
    handler = PUBHandler(pub)
    return handler


# TODO: if the logging directory does not exist create it
def setup_logging(default_path='logging.yaml', default_level='INFO', env_key='GUARDIAN_LOG_CFG'):
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    path = f'{os.path.dirname(os.path.realpath(__file__))}/{path}'
    if os.path.exists(path):
        with open(path, 'rt') as f:
            configset = yaml.safe_load(f.read())
            config.dictConfig(configset)
    else:
        log_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'logs/guardian.log')
        basicConfig(format='%(asctime)s: %(name)s: %(levelname)s: %(message)s', filename=log_path,
                    level=default_level)
        captureWarnings(True)


# TODO: what happens if I give a non existent file
def read_config(default_path='config_broker.yaml', subsection=None, env_key='GUARDIAN_NODE_CFG'):
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    # if the given path is not absolute then we will assume it is relative to this files' directory
    if not os.path.isabs(path):
        path = f'{os.path.dirname(os.path.realpath(__file__))}/{path}'
    if os.path.exists(path):
        try:
            with open(path, 'rt') as f:
                return yaml.safe_load(f.read()) if subsection is None else yaml.safe_load(f.read())[subsection]
        except yaml.YAMLError as exc:
            _LOG.error(f"error while reading config file: {exc}")
        except KeyError as _:
            _LOG.error(f"requested key [{subsection}] doesnt exist")
    return


if __name__ == "__main__":
    parser = ArgumentParser(description='GUARDIAN v%s' % __version__)
    parser.add_argument("-c", "--config", type=str, default='config/config_broker.yaml',
                        help="the absolute or relative path to a GUARDIAN configuration file")
    args = parser.parse_args()

    setup_logging()
    _LOG = getLogger('guardian')

    try:
        # read the configuration file if any is given
        file_configs = read_config(args.config)

        if not file_configs:
            parser.error("No configuration file could be parsed.")

        # TODO: Error handling and default values
        # TODO: SSH https://pyzmq.readthedocs.io/en/latest/ssh.html
        mode = file_configs.get("Node", {}).get("mode")
        name = file_configs.get("Node", {}).get("name")
        verbosity = file_configs.get("Node", {}).get("verbosity")
        quiet = file_configs.get("Node", {}).get("quiet")
        service = file_configs.get("Node", {}).get("service", [])

        transport = file_configs.get("MainEndpoint", {}).get("transport")
        host = file_configs.get("MainEndpoint", {}).get("host")
        port = file_configs.get("MainEndpoint", {}).get("port")

        opt_transport = file_configs.get("OptionalEndpoint", {}).get("transport")
        opt_host = file_configs.get("OptionalEndpoint", {}).get("host")
        opt_port = file_configs.get("OptionalEndpoint", {}).get("port")

        broadcast_transport = file_configs.get("BroadcastEndpoint", {}).get("transport")
        broadcast_host = file_configs.get("BroadcastEndpoint", {}).get("host")
        broadcast_port = file_configs.get("BroadcastEndpoint", {}).get("port")

        if quiet:
            # remove the stream handler
            for h in _LOG.handlers:
                if isinstance(h, StreamHandler):
                    _LOG.removeHandler(h)

        # set logging level
        raw_log_level = verbosity
        if raw_log_level <= 0:  # default
            log_level = CRITICAL
        elif raw_log_level == 1:
            log_level = ERROR
        elif raw_log_level == 2:
            log_level = WARNING
        elif raw_log_level == 3:
            log_level = INFO
        else:
            log_level = DEBUG
        _LOG.setLevel(log_level)

        if mode == 'broker':
            br(transport, host, port,
               opt_transport, opt_host, opt_port,
               broadcast_transport, broadcast_host, broadcast_port,
               name)
        elif mode == 'worker':
            wr(transport, host, port, name, service)
        elif mode == 'client':
            cr(transport, host, port,
               broadcast_transport, broadcast_host, broadcast_port,
               name)
        else:
            parser.error(f"Selected mode '{mode}' not recognized.\n\tAvailable options are [broker, worker, client].")

    except KeyboardInterrupt:
        pass
