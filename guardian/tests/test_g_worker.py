# -*- coding: utf-8 -*-
import sys
import time
from unittest import TestCase

import zmq
from zmq.eventloop.zmqstream import ZMQStream
from tornado.ioloop import IOLoop, PeriodicCallback

from guardian.g_worker import GWorker, ConnectionNotReadyError, MissingHeartbeat
from guardian.globals import *

_do_print = True


class WorkerRunner(GWorker):
    HB_INTERVAL = 1000
    HB_RETRIES = 10

    def on_request(self, msg):
        if _do_print:
            print("New worker request, replying with:", msg)
        answer = [b'REPLY'] + msg
        self.reply(answer)
        return


class TestGWorker(TestCase):

    endpoint = b'tcp://127.0.0.1:5555'
    service = b'test'

    def setUp(self):
        if _do_print:
            print('Setting up...')
        sys.stdout.flush()
        self.context = zmq.Context()
        self.broker = None
        self._msgs = []
        return

    def tearDown(self):
        if _do_print:
            print('Tearing down...')
        sys.stdout.flush()
        if self.broker:
            self._stop_broker()
        self.broker = None
        self.context = None
        return

    def _on_msg(self, msg):
        if _do_print:
            print('Broker received:', msg)
        self.target = msg.pop(0)
        marker_frame = msg.pop(0)
        if msg[1] == MSG_READY:  # ready
            if _do_print:
                print('READY received')
            return
        if msg[1] == MSG_HEARTBEAT:  # heartbeat
            if _do_print:
                print('HB received')
            return
        if msg[1] == MSG_REPLY:  # reply
            IOLoop.instance().stop()
            return
        return

    def _start_broker(self, do_reply=False):
        """Helper activating a fake broker in the ioloop.
        """
        socket = self.context.socket(zmq.ROUTER)
        self.broker = ZMQStream(socket)
        self.broker.socket.setsockopt(zmq.LINGER, 0)
        self.broker.bind(self.endpoint)
        self.broker.on_recv(self._on_msg)
        self.broker.do_reply = do_reply
        self.broker.ticker = PeriodicCallback(self._tick, WorkerRunner.HB_INTERVAL)
        self.broker.ticker.start()
        self.target = None
        if _do_print:
            print("Broker started")
        return

    def _stop_broker(self):
        if self.broker:
            self.broker.ticker.stop()
            self.broker.ticker = None
            self.broker.socket.close()
            self.broker.close()
            self.broker = None
        if _do_print:
            print("Broker stopped")
        return

    def _tick(self):
        if self.broker and self.target:
            msg = [self.target, b'', WORKER_PROTO, MSG_HEARTBEAT]
            self.broker.send_multipart(msg)
            if _do_print:
                print("Tick sent:", msg)
        return

    def send_req(self):
        data = [b'AA', b'BB']
        msg = [self.target, b'', WORKER_PROTO, MSG_QUERY, self.target, b''] + data
        self.broker.send_multipart(msg)
        if _do_print:
            print('broker sent:', msg)
        return

    @staticmethod
    def stop_loop():
        IOLoop.instance().stop()
        return

    # Tests follow

    def test_simple_worker(self):
        """Test GWorker simple req/reply.
        """
        self._start_broker()
        time.sleep(0.2)
        worker = WorkerRunner(self.context, self.endpoint, self.service)

        IOLoop.current().call_later(500, self.send_req)
        IOLoop.current().call_later(2500, self.stop_loop)

        IOLoop.current().instance().start()
        worker.shutdown()
        self._stop_broker()
        return
