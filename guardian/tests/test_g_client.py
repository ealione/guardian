# -*- coding: utf-8 -*-
import sys

import zmq
from tornado.ioloop import IOLoop
from zmq.eventloop.zmqstream import ZMQStream
from g_client import GClient, InvalidStateError
from unittest import TestCase

_do_print = True


class MyClient(GClient):

    def on_message(self, msg):
        if _do_print:
            print('Client received:', msg)
        self.last_msg = msg
        IOLoop.instance().stop()
        return

    def on_timeout(self):
        if _do_print:
            print('Client timed out!')
        IOLoop.instance().stop()
        return


class TestGClient(TestCase):
    endpoint = b'tcp://127.0.0.1:5555'
    service = b'test'

    def setUp(self):
        if _do_print:
            print('Setting up...')
        sys.stdout.flush()
        self.context = zmq.Context()
        self.broker = None
        self._msgs = []
        return

    def tearDown(self):
        if _do_print:
            print('Tearing down...')
        sys.stdout.flush()
        if self.broker:
            self._stop_broker()
        self.broker = None
        self._msgs = []
        self.context.term()
        self.context = None
        return

    def _on_msg(self, msg):
        self._msgs.append(msg)
        if _do_print:
            print('Broker received:', msg)
        sys.stdout.flush()
        if self.broker.do_reply:
            new_msg = msg[:4]
            new_msg.append(b'REPLY')
            self.broker.send_multipart(new_msg)
        else:
            IOLoop.instance().stop()
        return

    def _start_broker(self, do_reply=False):
        """Helper activating a fake broker in the ioloop.
        """
        if _do_print:
            print('Starting broker at', self.endpoint)
        sys.stdout.flush()
        socket = self.context.socket(zmq.ROUTER)
        self.broker = ZMQStream(socket)
        self.broker.socket.setsockopt(zmq.LINGER, 0)
        self.broker.bind(self.endpoint)
        self.broker.on_recv(self._on_msg)
        self.broker.do_reply = do_reply
        return

    def _stop_broker(self):
        if _do_print:
            print('Stopping broker')
        sys.stdout.flush()
        if self.broker:
            self.broker.close()
            self.broker = None
        return

    # Tests from here

    def test_01_create_01(self):
        """Test GClient simple create.
        """
        client = GClient(self.context, self.endpoint, self.service)
        self.assertEqual(self.endpoint, client.endpoint)
        self.assertEqual(self.service, client.service)
        client.shutdown()
        return

    def test_02_send_01(self):
        """Test GClient simple request.
        """
        self._start_broker()
        client = GClient(self.context, self.endpoint, self.service)
        client.request(b'XXX')
        IOLoop.instance().start()
        client.shutdown()
        self.assertEqual(len(self._msgs), 1)
        rmsg = self._msgs[0]
        # msg[0] is identity of sender
        self.assertEqual(rmsg[1], b'')  # routing delimiter
        self.assertEqual(rmsg[2], client._proto_version)
        self.assertEqual(rmsg[3], self.service)
        self.assertEqual(rmsg[4], b'XXX')
        self._stop_broker()
        return

    def test_02_send_02(self):
        """Test GClient multipart request.
        """
        mydata = [b'AAA', b'bbb']
        self._start_broker()
        client = GClient(self.context, self.endpoint, self.service)
        client.request(mydata)
        IOLoop.instance().start()
        client.shutdown()
        self.assertEqual(len(self._msgs), 1)
        rmsg = self._msgs[0]
        # msg[0] is identity of sender
        self.assertEqual(rmsg[1], b'')  # routing delimiter
        self.assertEqual(rmsg[2], client._proto_version)
        self.assertEqual(rmsg[3], self.service)
        self.assertEqual(rmsg[4:], mydata)
        self._stop_broker()
        return

    def test_02_send_03(self):
        """Test GClient request in invalid state.
        """
        client = GClient(self.context, self.endpoint, self.service)
        client.request(b'XXX')  # ok
        self.assertRaises(InvalidStateError, client.request, b'AAA')
        client.shutdown()
        return

    def test_03_timeout_01(self):
        """Test GClient request w/ timeout.
        """
        client = MyClient(self.context, self.endpoint, self.service)
        client.request(b'XXX', 2)  # 2 secs timeout
        IOLoop.instance().start()
        client.shutdown()
        self.assertEqual(client.timed_out, True)
        return

    def test_04_receive_01(self):
        """Test GClient message receive.
        """
        self._start_broker(do_reply=True)
        client = MyClient(self.context, self.endpoint, self.service)
        client.request(b'XXX')
        IOLoop.instance().start()
        client.shutdown()
        self._stop_broker()
        self.assertEqual(True, hasattr(client, 'last_msg'))
        self.assertEqual(4, len(client.last_msg))
        self.assertEqual(b'REPLY', client.last_msg[-1])
        self.assertEqual(self.service, client.last_msg[-2])
        return
