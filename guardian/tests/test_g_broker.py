# -*- coding: utf-8 -*-
import sys
import time
from unittest import TestCase

import zmq
from zmq.eventloop.zmqstream import ZMQStream
from tornado.ioloop import IOLoop, PeriodicCallback

from guardian.g_broker import GBroker
from guardian.globals import *

_do_print = True


class BrokerRunner(GBroker):
    HB_INTERVAL = 1000
    HB_RETRIES = 10


class TestGBroker(TestCase):

    endpoint = b'tcp://127.0.0.1:9093'
    opt_endpoint = b'tcp://127.0.0.1:9094'

    def setUp(self):
        if _do_print:
            print('Setting up...')
        sys.stdout.flush()
        self.context = zmq.Context()
        return

    def tearDown(self):
        if _do_print:
            print('Tearing down...')
        sys.stdout.flush()
        self.context = None
        return

    def test_register_worker(self):
        broker = BrokerRunner(self.context, self.endpoint, self.opt_endpoint)
        self.fail()

    def test_unregister_worker(self):
        self.fail()

    def test_disconnect(self):
        self.fail()

    def test_respond_to_client(self):
        self.fail()

    def test_shutdown(self):
        self.fail()

    def test_on_timer(self):
        self.fail()

    def test_on_ready(self):
        self.fail()

    def test_on_reply(self):
        self.fail()

    def test_on_heartbeat(self):
        self.fail()

    def test_on_disconnect(self):
        self.fail()

    def test_on_client(self):
        self.fail()

    def test_on_worker(self):
        self.fail()

    def test_on_message(self):
        self.fail()

    def test_find_worker(self):
        self.fail()

    def test_send_to_all_workers(self):
        self.fail()
