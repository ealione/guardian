# -*- coding: utf-8 -*-
# GENERAL CONFIGURATION
CLIENT_PROTO = b'GPC01'  #: Client protocol identifier
WORKER_PROTO = b'GPW01'  #: Worker protocol identifier
G_BROKER = b'Broker'
G_WORKER = b'Worker'
G_CLIENT = b'Client'

HB_INTERVAL = 3000      #: Heartbeat interval in milliseconds
HB_RETRIES = 3          #: Heartbeat retries before quiting
HB_RETRY_INTERVAL = 3   #: Reconnection interval in seconds
HB_PUBLISH_INTERVAL = 3000

DEFAULT_INTEFACE = 'enp2s0'

# MSG TYPES
MSG_READY = b'\x01'
MSG_QUERY = b'\x02'
MSG_REPLY = b'\x03'
MSG_HEARTBEAT = b'\x04'
MSG_DISCONNECT = b'\x05'
MSG_INFO_DUMP = b'\x06'

# WORKER STATUS
WORKER_OFFLINE_STATUS = b'lost'
WORKER_ONLINE_STATUS = b'online'
WORKER_BUSY_STATUS = b'busy'
WORKER_INACTIVE_STATUS = b'inactive'

# SERVICES
SERVICE_ECHO = 'echo'
SERVICE_BROKER = 'broker'
