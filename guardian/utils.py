# -*- coding: utf-8 -*-
import json
import base64
import binascii
import psutil
import zmq
import socket

from sys import getsizeof
from hashlib import sha256
from random import randint
from netifaces import interfaces, ifaddresses, AF_INET
from types import ModuleType, FunctionType
from gc import get_referents

transports = frozenset(['udp', 'tcp', 'ipc', 'inproc'])
blacklist = type, ModuleType, FunctionType


def socket_set_hwm(socket, hwm=-1):
    """libzmq 2/3/4 compatible sethwm"""
    try:
        socket.sndhwm = socket.rcvhwm = hwm
    except AttributeError:
        socket.hwm = hwm


def dump(msg_or_socket):
    """Receives all message parts from socket, printing each frame neatly"""
    if isinstance(msg_or_socket, zmq.Socket):
        # it's a socket, call on current message
        msg = msg_or_socket.recv_multipart()
    else:
        msg = msg_or_socket
    print("----------------------------------------")
    for part in msg:
        print("[%03d]" % len(part), end=' ')
        try:
            print(part.decode('ascii'))
        except UnicodeDecodeError:
            print(r"0x%s" % (binascii.hexlify(part).decode('ascii')))


def socket_set_id(zsocket):
    """Set simple random printable identity on socket"""
    identity = u"%04x-%04x" % (randint(0, 0x10000), randint(0, 0x10000))
    zsocket.setsockopt_string(zmq.IDENTITY, identity)


def zmq_addr(port, transport=None, host=None):
    if host is None:
        host = '127.0.0.1'
    else:
        socket.inet_aton(host)

    if transport is None:
        transport = 'tcp'

    assert transport in transports
    assert 1000 < port < 10000

    return f'{transport}://{host}:{port}'.encode()


def socketid2hex(sid):
    """Returns printable hex representation of a socket id.
    """
    ret = ''.join("%02X" % ord(c) for c in sid)
    return ret


def split_address(msg):
    """Function to split return Id and message received by ROUTER socket.

    Returns 2-tuple with return Id and remaining message parts.
    Empty frames after the Id are stripped.
    """
    ret_ids = []
    for i, p in enumerate(msg):
        if p:
            ret_ids.append(p)
        else:
            break
    return ret_ids, msg[i + 1:]


def hash_b64(s):
    """
    Returns the base 64 hash of `s`
    """
    hasher = sha256(s)
    result = base64.b64encode(hasher.digest())[:-1]
    return result


def to_json(d):
    """
    Serializes an object into a json string
    """
    return json.dumps(d, sort_keys=True)


def from_json(json_str):
    """
    Returns an object from a json string
    """
    if not json_str: return None
    return json.loads(json_str)


def bytes_to_hexstring(raw_bytes):
    """
    Returns a hexadecimal representation of a bytearray
    """
    hex_data = binascii.hexlify(raw_bytes)
    text_string = hex_data.decode('utf-8')
    return text_string


def hexstring_to_bytes(hexstring):
    """
    Converts a hex encoded string to a bytearray
    """
    try:
        raw_bytes = binascii.unhexlify(hexstring.encode('utf-8'))
    except TypeError:
        return None
    return raw_bytes


def get_ip_addresses(family=AF_INET):
    """
    Returns 2-tuple with interface address.
    """
    for interface, snics in psutil.net_if_addrs().items():
        for snic in snics:
            if snic.family == family:
                yield interface, snic.address


def get_current_ip(intface=None):
    """
    Will return the current ip address for the given interface.

    If no interface is given the wireless interface will be chosen.

    .. note::

           If the wireless interface name does not exist or no explicit
           interface is given this function will return the ip of the
           last available interface.
    """
    intface_names = interfaces()
    intface_last = intface_names[-1]
    if intface is None or intface not in intface_names:
        intface = intface_last
    return ifaddresses(intface)[AF_INET][0]['addr']


def getsize(obj):
    """sum size of object & members."""
    if isinstance(obj, blacklist):
        raise TypeError(f'getsize() does not take argument of type: {str(type(obj))}')
    seen_ids = set()
    size = 0
    objects = [obj]
    while objects:
        need_referents = []
        for obj in objects:
            if not isinstance(obj, blacklist) and id(obj) not in seen_ids:
                seen_ids.add(id(obj))
                size += getsizeof(obj)
                need_referents.append(obj)
        objects = get_referents(*need_referents)
    return size
