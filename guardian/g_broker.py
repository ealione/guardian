# -*- coding: utf-8 -*-
from __future__ import annotations
from logging import getLogger

import zmq
from tornado.ioloop import PeriodicCallback
from zmq.eventloop.zmqstream import ZMQStream

from typing import List, Optional, ByteString, Any

from globals import *
from g_obj import G_Object
from utils import split_address, bytes_to_hexstring, socket_set_hwm, socket_set_id, get_current_ip
from g_data import MessageDataType

_LOG = getLogger('guardian')


class GBroker(G_Object):
    # TODO: add two brokers using b-star
    # TODO: implement the titanic scheme for added reliability in case of disjoint req/rep
    def __init__(self, context: zmq.Context, main_ep: ByteString, opt_ep: Optional[ByteString] = None,
                 service_q: Optional[ServiceQueue] = None, hb_interval: int = 3000) -> None:
        # _LOG.name = f"{'Broker'}({get_current_ip()})"
        if service_q is None:
            self.service_q = ServiceQueue
        else:
            self.service_q = service_q

        socket = context.socket(zmq.ROUTER)
        socket.bind(main_ep)
        socket_set_id(socket)
        socket_set_hwm(socket, 1000)
        self.main_stream = ZMQStream(socket)
        self.main_stream.on_recv(self.on_message)
        if opt_ep:
            socket = context.socket(zmq.ROUTER)
            socket.bind(opt_ep)
            socket_set_id(socket)
            socket_set_hwm(socket, 1000)
            self.client_stream = ZMQStream(socket)
            self.client_stream.on_recv(self.on_message)
        else:
            self.client_stream = self.main_stream

        self._workers = {}
        self.services = {}
        self._worker_cmds = {
            MSG_READY: self.on_ready,
            MSG_REPLY: self.on_reply,
            MSG_HEARTBEAT: self.on_heartbeat,
            MSG_DISCONNECT: self.on_disconnect
        }
        self.hb_check_timer = PeriodicCallback(self.on_timer, hb_interval)
        self.hb_check_timer.start()

        _LOG.info(f"Initialized successfully with main endpoint [{main_ep.decode('utf-8')}] "
                  f"and optional endpoint [{opt_ep.decode('utf-8')}].")
        return

    def register_worker(self, wid: ByteString, service: ByteString) -> None:
        if wid in self._workers:
            return
        self._workers[wid] = WorkerTracker(WORKER_PROTO, wid, service, self.main_stream)
        # If service exists then add this worker to its workers queue, if not create it.
        if service in self.services:
            wq, wr = self.services[service]
            wq.put(wid)
        else:
            q = self.service_q()
            q.put(wid)
            self.services[service] = (q, [])
        _LOG.info(f"New worker [{bytes_to_hexstring(wid)}] registered for service [{service.decode('utf-8')}].")
        return

    def unregister_worker(self, wid: ByteString) -> None:
        try:
            wtracker = self._workers[wid]
        except KeyError:
            # not registered, ignore
            return
        wtracker.shutdown()
        service = wtracker.service
        if service in self.services:
            wq, wr = self.services[service]
            wq.remove(wid)
        del self._workers[wid]
        _LOG.info(f"Worker [{bytes_to_hexstring(wid)}] was removed.")
        return

    def disconnect(self, wid: ByteString) -> None:
        try:
            _ = self._workers[wid]
        except KeyError:
            # not registered, ignore
            return
        _LOG.info(f"Requesting from worker [{bytes_to_hexstring(wid)}] to disconnect.")
        to_send = [wid, b'', WORKER_PROTO, MSG_DISCONNECT]
        if self.main_stream.closed():
            self.shutdown()
        self.main_stream.send_multipart(to_send)
        self.unregister_worker(wid)
        return

    def respond_to_client(self, rp: MessageDataType, service: ByteString, cmd: ByteString, wid: ByteString,
                          msg: MessageDataType) -> None:
        _LOG.debug(f"Replying to client {rp} regarding request [{cmd}].")
        to_send = rp[:]
        to_send.extend([b'', CLIENT_PROTO, service, cmd, str(wid)])
        to_send.extend(msg)
        if self.client_stream.closed():
            self.shutdown()
        self.client_stream.send_multipart(to_send)
        return

    def shutdown(self) -> None:
        if self.client_stream == self.main_stream:
            self.client_stream = None
        self.main_stream.on_recv(None)
        self.main_stream.socket.setsockopt(zmq.LINGER, 0)
        self.main_stream.close()
        self.main_stream = None
        if self.client_stream:
            self.client_stream.on_recv(None)
            self.client_stream.socket.setsockopt(zmq.LINGER, 0)
            self.client_stream.close()
            self.client_stream = None
        self._workers = {}
        self.services = {}
        _LOG.info("Shutting down! All workers unregistered, will not process more messages.")
        return

    def on_timer(self) -> None:
        for wtracker in list(self._workers.values()):
            if not wtracker.is_alive():
                _LOG.debug(f"Worker [{bytes_to_hexstring(wtracker.id)}] timed out.")
                self.unregister_worker(wtracker.id)
        return

    def on_ready(self, rp: MessageDataType, msg: MessageDataType) -> None:
        try:
            ret_id = rp[0]
            service = msg.pop(0)
            self.register_worker(ret_id, service)
        except IndexError:
            _LOG.debug(f"Error while registering worker [{rp}].")
        return

    def on_reply(self, rp: MessageDataType, msg: MessageDataType) -> None:
        ret_id = rp[0]
        wtracker = self._workers.get(ret_id)
        if not wtracker:
            # worker not found, ignore message
            return
        service = wtracker.service
        # make worker available again
        try:
            wq, wr = self.services[service]
            cp, msg = split_address(msg)
            cmd = msg.pop(0)
            self.respond_to_client(cp, service, cmd, bytes_to_hexstring(wtracker.id), msg)
            wq.put(wtracker.id)
            if wr:
                proto, rp, msg = wr.pop(0)
                self.on_client(proto, rp, msg)
        except KeyError:
            # unknown service
            _LOG.info(f"Worker [{bytes_to_hexstring(ret_id)}] reports an unknown service.")
            self.disconnect(ret_id)
        return

    def on_heartbeat(self, rp: MessageDataType, _) -> None:
        ret_id = rp[0]
        try:
            worker = self._workers[ret_id]
            if worker.is_alive():
                worker.on_heartbeat()
        except KeyError:
            # ignore HB for unknown worker
            pass
        return

    def on_disconnect(self, rp: MessageDataType, _) -> None:
        wid = rp[0]
        _LOG.debug(f"Worker [{bytes_to_hexstring(wid)}] wants to disconnect.")
        self.unregister_worker(wid)
        return

    def on_ho(self, rp: MessageDataType, service: ByteString, msg: MessageDataType):
        _LOG.debug(f"New HO request received for service [{service.decode('utf-8')}] and code [{msg}].")
        if service == b'ho.service':
            s = msg[0]
            ret = b'404'
            # TODO: review this
            for wr in self._workers.values():
                if s == wr.service:
                    ret = b'200'
                    break
            self.respond_to_client(rp, service, b'', b'', [ret])
        else:
            self.respond_to_client(rp, service, b'', b'', [b'501'])
        return

    def on_client(self, proto: MessageDataType, rp: MessageDataType, msg: MessageDataType):
        # TODO: Optimize this functin
        _LOG.debug(f"Received a new request from client [{bytes_to_hexstring(rp[0])}] regarding [{msg[0]}].")
        service = msg.pop(0)
        if service == SERVICE_BROKER:
            self.on_ho(rp, service, msg)
            return
        try:
            wq, wr = self.services[service]
            cwid = msg.pop(0)
            wid = self.find_worker(cwid, service)
            if not wid:
                # no worker ready
                # queue message
                msg.insert(0, cwid)
                msg.insert(0, service)
                wr.append((proto, rp, msg))
                return
            wtracker = self._workers[wid]
            to_send = [wtracker.id, b'', WORKER_PROTO, MSG_QUERY]
            to_send.extend(rp)
            to_send.append(b'')
            to_send.extend(msg)
            if self.main_stream.closed():
                self.shutdown()
            self.main_stream.send_multipart(to_send)
        except KeyError:
            # unknwon service
            # ignore request
            _LOG.debug(f"Broker has no service [{service}].")
        return

    def on_worker(self, proto: MessageDataType, rp: MessageDataType, msg: MessageDataType) -> None:
        _LOG.debug(f"Received a new reply from worker [{bytes_to_hexstring(rp[0])}].")
        cmd = msg.pop(0)
        if cmd in self._worker_cmds:
            fnc = self._worker_cmds[cmd]
            fnc(rp, msg)
        else:
            # ignore unknown command
            # DISCONNECT worker
            _LOG.info(f"Worker [{bytes_to_hexstring(rp[0])}] is trying to use an unknown command.")
            self.disconnect(rp[0])
        return

    def on_message(self, msg: MessageDataType) -> None:
        _LOG.debug(f"Received new message {msg}.")
        rp, msg = split_address(msg)
        # TODO: Sanitize message
        # dispatch on first frame after path
        t = msg.pop(0)
        if t == WORKER_PROTO:
            self.on_worker(t, rp, msg)
        elif t == CLIENT_PROTO:
            self.on_client(t, rp, msg)
        else:
            _LOG.debug(f"Received unknown Protocol: '{t}'")
        return

    def find_worker(self, wid: ByteString, service: ByteString) -> Optional[: MessageDataType]:
        wq, wr = self.services[service]
        if wid in wq:
            wq.remove(wid)
            _LOG.debug(f"Worker [{wid}] selected and removed from the pool.")
            return wid
        else:
            return None

    def send_to_all_workers(self, rp: MessageDataType, msg_type: ByteString = MSG_INFO_DUMP) -> None:
        for wid in self._workers:
            to_send = [wid, b'', WORKER_PROTO, MSG_QUERY, rp, b'', msg_type]
            if self.main_stream.closed():
                self.shutdown()
            self.main_stream.send_multipart(to_send)


class WorkerTracker(object):
    def __init__(self, proto: ByteString, wid: ByteString, service: ByteString, stream: ZMQStream) -> None:
        self.proto = proto
        self.id = wid
        self.service = service
        self.curr_retries = HB_RETRIES
        self.stream = stream
        self.last_hb = 0
        self.data: List[Any] = []
        self.hb_out_timer = PeriodicCallback(self.send_hb, HB_INTERVAL)
        self.hb_out_timer.start()
        return

    def send_hb(self) -> None:
        self.curr_retries -= 1
        msg = [self.id, b'', self.proto, MSG_HEARTBEAT]
        if self.stream.closed():
            return
        self.stream.send_multipart(msg)
        return

    def set_stream(self, stream: ZMQStream) -> None:
        self.stream = stream

    def on_heartbeat(self) -> None:
        self.curr_retries = HB_RETRIES

    def is_alive(self) -> bool:
        return self.curr_retries > 0

    def shutdown(self):
        self.hb_out_timer.stop()
        self.hb_out_timer = None
        self.stream = None


class ServiceQueue(object):
    def __init__(self) -> None:
        self.q: List = []
        return

    def __contains__(self, wid: ByteString) -> bool:
        return wid in self.q

    def __len__(self) -> int:
        return len(self.q)

    def remove(self, wid: ByteString) -> None:
        try:
            self.q.remove(wid)
        except ValueError:
            pass

    def put(self, wid: ByteString) -> None:
        if wid not in self.q:
            self.q.append(wid)

    def get(self) -> Optional[WorkerTracker]:
        if not self.q:
            return None
        return self.q.pop(0)
