import zlib
import pickle
import msgpack
import zmq
import numpy


class GuardianSocket(zmq.Socket):
    def send_broker_data_publish(self, topic, obj, flags=0):
        obj = msgpack.packb(obj)
        return self.send_multipart([topic, obj], flags=flags)

    def recv_broker_data_publish(self, flags=0):
        msg = self.recv_multipart(flags)
        topic = msg.pop(0)
        msg = msgpack.unpackb(msg)
        return [topic, msg]

    def send_zipped_pickle(self, obj, flags=0, protocol=-1):
        pobj = pickle.dumps(obj, protocol)
        zobj = zlib.compress(pobj)
        return self.send(zobj, flags=flags)

    def recv_zipped_pickle(self, flags=0):
        zobj = self.recv(flags)
        pobj = zlib.decompress(zobj)
        return pickle.loads(pobj)

    def send_array(self, A, flags=0, copy=True, track=False):
        """send a numpy array with metadata"""
        md = dict(
            dtype=str(A.dtype),
            shape=A.shape,
        )
        self.send_json(md, flags | zmq.SNDMORE)
        return self.send(A, flags, copy=copy, track=track)

    def recv_array(self, flags=0, copy=True, track=False):
        """recv a numpy array"""
        md = self.recv_json(flags=flags)
        msg = self.recv(flags=flags, copy=copy, track=track)
        A = numpy.frombuffer(msg, dtype=md['dtype'])
        return A.reshape(md['shape'])


class GuardianContext(zmq.Context):
    _socket_class = GuardianSocket


def test():
    ctx = GuardianContext()
    req = ctx.socket(zmq.REQ)
    rep = ctx.socket(zmq.REP)

    rep.bind('inproc://a')
    req.connect('inproc://a')
    A = numpy.ones((1024, 1024))
    print("Array is %i bytes" % (A.nbytes))

    # send/recv with pickle+zip
    req.send_zipped_pickle(A)
    B = rep.recv_zipped_pickle()
    # now try non-copying version
    rep.send_array(A, copy=False)
    C = req.recv_array(copy=False)
    print("Checking zipped pickle...")
    print("Okay" if (A == B).all() else "Failed")
    print("Checking send_array...")
    print("Okay" if (C == B).all() else "Failed")


if __name__ == '__main__':
    test()
