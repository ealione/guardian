import msgpack
import pickle
from dataclasses import dataclass
from typing import List, ByteString

from utils import from_json, to_json

MessageDataType = List[ByteString]


@dataclass
class MessageData:
    data: MessageDataType = None

    @classmethod
    def from_recv(cls, msg: MessageDataType):
        md = MessageData()
        md.deserialize_object(msg)
        return md

    def serialize_object(self, protocol=-1):
        p = pickle.dumps(self.data, protocol)
        # p = to_json(self.data)
        return msgpack.packb(p)

    def deserialize_object(self, msg):
        p = msgpack.unpackb(msg)
        # p = from_json(msg)
        self.data = pickle.loads(p)
