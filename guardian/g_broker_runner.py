# -*- coding: utf-8 -*-
import msgpack
import zmq
from tornado.ioloop import PeriodicCallback
from tornado.ioloop import IOLoop
from logging import getLogger

from typing import ByteString

from g_broker import GBroker
from globals import HB_INTERVAL, HB_PUBLISH_INTERVAL
from utils import zmq_addr, get_current_ip, socket_set_hwm

_LOG = getLogger("guardian")


class GuardianRunner(GBroker):
    def __init__(self, context: zmq.Context, address: ByteString, opt_address: ByteString,
                 broadcast_address: ByteString, name: str) -> None:
        _LOG.name = f"{name.upper()} ({get_current_ip()})"

        GBroker.__init__(self, context, address, opt_address, hb_interval=HB_INTERVAL)

        self.context = context
        self.broadcast_address = broadcast_address
        self.publisher = None

        self.create_broadcast_stream()
        self.publish = PeriodicCallback(self.publish_updates, HB_PUBLISH_INTERVAL)
        self.publish.start()

        _LOG.info(f"Broadcasting at [{broadcast_address}].")

        return

    def create_broadcast_stream(self):
        self.publisher = self.context.socket(zmq.PUB)
        self.publisher.bind(self.broadcast_address)
        socket_set_hwm(self.publisher, 1000)

    def publish_updates(self):
        # TODO: Do we need a broadcasting protocol id?
        for service, workers in self.services.items():
            msg = msgpack.packb(workers[0].q)
            _LOG.debug(f"Broadcasting topic [{service}]")
            try:
                self.publisher.send_multipart([service, msg])
            except zmq.ZMQError as e:
                if e.errno == zmq.ETERM:
                    raise

    def close_broker(self):
        self.shutdown()
        self.publish.stop()
        if self.publisher:
            self.publisher.setsockopt(zmq.LINGER, 0)
            self.publisher.close()
            self.publisher = None


def run(transport: str, host: str, port: str,
        opt_transport: str, opt_host: str, opt_port: str,
        broadcast_transport: str, broadcast_host: str, broadcast_port: str,
        name: str) -> None:
    context = zmq.Context()
    address = zmq_addr(port, transport, host)
    opt_address = zmq_addr(opt_port, opt_transport, opt_host)
    broadcast_address = zmq_addr(broadcast_port, broadcast_transport, broadcast_host)
    broker = GuardianRunner(context, address, opt_address, broadcast_address, name)
    try:
        IOLoop.instance().start()
    except KeyboardInterrupt:
        _LOG.info("Interrupt received, stopping.")
    finally:
        # clean up
        broker.close_broker()
        context.term()
        IOLoop.instance().stop()
