# -*- coding: utf-8 -*-
from logging import getLogger

import msgpack
import zmq
from tornado.ioloop import IOLoop
from zmq.eventloop.zmqstream import ZMQStream

from typing import ByteString

from globals import *
from g_obj import G_Object
from g_client import GClient, mn_request
from utils import zmq_addr, get_current_ip, socket_set_hwm
from g_data import MessageDataType

_LOG = getLogger("guardian")


# https://github.com/Robpol86/terminaltables
# https://github.com/mkaz/termgraph
# https://github.com/amanusk/s-tui
# https://github.com/urwid/urwid
# http://sureskumar.com/wp-content/uploads/2014/02/IMG_5298_web.jpg

# https://github.com/celery/celery/tree/master/celery


class ReqClient(G_Object):
    def __init__(self, transport: str, host: str, port: str):
        self.endpoint = zmq_addr(port, transport, host)
        self.context = zmq.Context()
        self.reply = None
        msg = [b'TEST']
        service = b'echo'
        timeout = 2.0

        if self.request_with_timeout(service, msg, timeout):
            print(self.reply)
        else:
            print("Timeout!")

    def request_with_timeout(self, service, msg, timeout):
        socket = self.context.socket(zmq.REQ)
        socket.setsockopt(zmq.LINGER, 0)
        socket.connect(self.endpoint)
        res = mn_request(socket, service, msg, timeout)
        if res:
            self.reply = repr(res)
        socket.close()
        return bool(res)

    def shutdown(self):
        self.context.term()


class ClientRunner(GClient):
    def __init__(self, context: zmq.Context, address: ByteString, broadcast_address: ByteString, name: str,
                 service: str) -> None:
        _LOG.name = f"{name.upper()} ({get_current_ip()})"

        GClient.__init__(self, context, address, service.encode())

        self.context = context
        self.publisher_stream = None
        self.broadcast_address = broadcast_address
        self.name = name

        self.connect_to_publisher()
        self.subscribe(service)
        return

    def on_message(self, msg: MessageDataType) -> None:
        # https://github.com/zeromq/pyzmq/blob/master/examples/eventloop/coroutines.py
        _LOG.debug(f"Received new message {msg}.")
        # 1st part is empty
        msg.pop(0)
        # 2nd part is protocol version
        # TODO: Version check
        proto = msg.pop(0)
        if proto != CLIENT_PROTO:
            return

    def on_timeout(self):
        _LOG.info("Timeout! No response from broker.")
        return

    def connect_to_publisher(self):
        socket = self.context.socket(zmq.SUB)
        socket.connect(self.broadcast_address)
        socket_set_hwm(socket, 1000)
        self.publisher_stream = ZMQStream(socket)
        self.publisher_stream.on_recv(self.on_message)

        _LOG.info(f"Connected for updates at [{self.broadcast_address.decode('utf-8')}]")

    def subscribe_list(self, topics):
        for topic in topics:
            self.subscribe(topic)

    def subscribe(self, topic):
        _LOG.debug(f"Subscribing to topic [{topic}].")
        self.filter_op(zmq.SUBSCRIBE, topic)

    def unsubscribe_list(self, topics):
        for topic in topics:
            self.unsubscribe(topic)

    def unsubscribe(self, topic):
        _LOG.debug(f"Trying to unsubscribe from topic [{topic}].")
        self.filter_op(zmq.UNSUBSCRIBE, topic)

    def filter_op(self, op, topic):
        assert op in [zmq.SUBSCRIBE, zmq.UNSUBSCRIBE]
        if not isinstance(topic, bytes):
            topic = topic.encode('utf-8')
        self.publisher_stream.setsockopt(op, topic)

    def close_client(self):
        self.shutdown()
        if not self.publisher_stream:
            return
        _LOG.info("Closing publisher stream.")
        self.publisher_stream.close()
        self.publisher_stream = None


def run(transport: str, host: str, port: str,
        broadcast_transport: str, broadcast_host: str, broadcast_port: str,
        name: str) -> None:
    context = zmq.Context()
    address = zmq_addr(port, transport, host)
    broadcast_address = zmq_addr(broadcast_port, broadcast_transport, broadcast_host)
    client = ClientRunner(context, address, broadcast_address, name, SERVICE_ECHO)
    try:
        IOLoop.instance().start()
        client.shutdown()
    except KeyboardInterrupt:
        _LOG.info("Interrupt received, stopping!")
    finally:
        # clean up
        # TODO: Fix shutdown
        client.close_client()
        context.term()
        IOLoop.current().stop()
