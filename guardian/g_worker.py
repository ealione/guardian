# -*- coding: utf-8 -*-
import zmq
from zmq.eventloop.zmqstream import ZMQStream
from tornado.ioloop import IOLoop, PeriodicCallback
from utils import split_address
from logging import getLogger

from typing import Optional, ByteString

from globals import *
from g_obj import G_Object
from utils import getsize, get_current_ip
from g_data import MessageDataType

_LOG = getLogger('guardian')


class ConnectionNotReadyError(RuntimeError):
    """Exception raised when attempting to use the MNWorker before the handshake took place.
    """
    pass


class MissingHeartbeat(UserWarning):
    """Exception raised when a heartbeat was not received on time.
    """
    pass


class GWorker(G_Object):
    def __init__(self, context: zmq.Context, endpoint: ByteString, service: ByteString, hb_retries: int = 3,
                 hb_interval: int = 3000, reconnection_interval: int = 3) -> None:
        # _LOG.name = f"{'Worker'}({get_current_ip()})"
        self.context = context
        self.endpoint = endpoint
        self.service = service
        self.envelope = None
        self.hb_retries = hb_retries
        self.hb_interval = hb_interval
        self.reconnection_interval = reconnection_interval
        self.stream: Optional[ZMQStream] = None
        self.timed_out: bool = False
        self.need_handshake: bool = True
        self.connected: bool = False
        self.ticker = None
        self._delayed_cb = None
        self._create_stream()

        _LOG.info(f"Initialized successfully and connected to [{endpoint.decode('utf-8')}].")
        return

    def _create_stream(self) -> None:
        _LOG.debug("Initializing streams.")
        ioloop = IOLoop.instance()
        socket = self.context.socket(zmq.DEALER)
        self.stream = ZMQStream(socket, ioloop)
        self.stream.on_recv(self._on_message)
        self.stream.socket.setsockopt(zmq.LINGER, 0)
        self.stream.connect(self.endpoint)
        self.ticker = PeriodicCallback(self._tick, self.hb_interval, .1)
        self._send_ready()
        self.ticker.start()
        return

    def _send_ready(self) -> None:
        _LOG.debug("Trying to register with broker.")
        ready_msg = [b'', WORKER_PROTO, MSG_READY, self.service]
        if self.stream.closed():
            self.shutdown()
        self.stream.send_multipart(ready_msg)
        self.curr_retries = self.hb_retries
        return

    def _tick(self) -> None:
        self.curr_retries -= 1
        self.send_hb()
        if self.curr_retries >= 0:
            return
        # connection seems to be dead
        _LOG.debug(f"Connection is down, shutting down and trying again after [{self.reconnection_interval}] seconds.")
        self.shutdown()
        # try to recreate it
        self._delayed_cb = IOLoop.current().call_later(self.reconnection_interval, self._create_stream)
        return

    def send_hb(self) -> None:
        _LOG.debug("Sending heartbeat.")
        msg = [b'', WORKER_PROTO, MSG_HEARTBEAT]
        if self.stream.closed():
            self.shutdown()
        self.stream.send_multipart(msg)
        return

    def shutdown(self) -> None:
        if self.ticker:
            self.ticker.stop()
            self.ticker = None
        if not self.stream:
            return
        self.stream.close()
        self.stream = None
        self.timed_out = False
        self.need_handshake = True
        self.connected = False
        return

    def reply(self, msg: MessageDataType) -> None:
        if self.need_handshake:
            raise ConnectionNotReadyError()
        to_send = self.envelope
        self.envelope = None
        if isinstance(msg, list):
            to_send.extend(msg)
        else:
            to_send.append(msg)
        if self.stream.closed():
            self.shutdown()
        self.stream.send_multipart(to_send)
        return

    def _on_message(self, msg: MessageDataType) -> None:
        _LOG.debug(f"Received: {msg}.")
        # 1st part is empty
        msg.pop(0)
        # 2nd part is protocol version
        proto = msg.pop(0)
        if proto != WORKER_PROTO:
            # ignore message from not supported protocol
            pass
        # 3rd part is message type
        msg_type = msg.pop(0)
        # any message resets the retries counter
        self.need_handshake = False
        self.curr_retries = self.hb_retries
        if msg_type == MSG_DISCONNECT:  # disconnect
            self.curr_retries = 0  # reconnect will be triggered by hb timer
        elif msg_type == MSG_QUERY:  # request
            # remaining parts are the user message
            envelope, msg = split_address(msg)
            envelope.append(b'')
            envelope = [b'', WORKER_PROTO, MSG_REPLY] + envelope  # reply
            self.envelope = envelope
            self.on_request(msg)
        else:
            # invalid message ignored
            _LOG.debug(f'Ignoring message with invalid id [{msg_type}].')
            pass
        return

    def on_request(self, msg: MessageDataType):
        """Public method called when a request arrived.

        Must be overloaded to provide support for various services!
        """
        pass


class DataTracker(object):
    def __init__(self, hid: ByteString, service: ByteString, contents: MessageDataType) -> None:
        self.hid = hid
        self.service = service
        self._contents = contents
        return

    def get_size(self) -> float:
        return getsize(self._contents)

    def __len__(self) -> int:
        return len(self._contents)
