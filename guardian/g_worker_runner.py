# -*- coding: utf-8 -*-
from logging import getLogger

import zmq
from tornado.ioloop import IOLoop

from globals import *
from g_worker import GWorker
from g_socket import GuardianContext
from utils import zmq_addr, get_current_ip
from g_data import MessageDataType

_LOG = getLogger("guardian")

# TODO: At some point this must be set dynamically
WK_SERVICE = SERVICE_ECHO


# TODO: If connection with broker is lost then restart
class WorkerRunner(GWorker):
    def __init__(self, context: zmq.Context, transport: str, host: str, port: str, name: str, service: str) -> None:
        _LOG.name = f"{name.upper()} ({get_current_ip()})"

        endpoint = zmq_addr(port, transport, host)
        GWorker.__init__(self, context, endpoint, service.encode(), HB_RETRIES, HB_INTERVAL, HB_RETRY_INTERVAL)

        self.name = name
        self._worker_funcs = {}

    def on_request(self, msg: MessageDataType) -> None:
        cmd = msg.pop(0)
        if cmd in self._worker_funcs:
            fnc = self._worker_funcs[cmd]
            self.reply([cmd, fnc(msg)])
        else:
            # ignore unknown command
            _LOG.info(f"I received an unknown command [{cmd}].")

    def shutdown(self) -> None:
        super(WorkerRunner, self).shutdown()


def run(transport: str, host: str, port: str, name: str, service: str) -> None:
    context = GuardianContext()
    worker = WorkerRunner(context, transport, host, port, name, service)
    try:
        IOLoop.instance().start()
        worker.shutdown()
    except KeyboardInterrupt:
        _LOG.info("Interrupt received, stopping!")
    finally:
        # clean up
        worker.shutdown()
        context.term()
        IOLoop.current().stop()
