# -*- coding: utf-8 -*-
import zmq
from zmq.eventloop.zmqstream import ZMQStream
from tornado.ioloop import IOLoop
from zmq import select
from logging import getLogger

from typing import ByteString, Optional

from globals import *
from g_obj import G_Object
from g_data import MessageDataType

_LOG = getLogger('guardian')


class InvalidStateError(RuntimeError):
    """Exception raised when the requested action is not available due to socket state.
    """
    pass


class RequestTimeout(UserWarning):
    """Exception raised when the request timed out.
    """
    pass


class GClient(G_Object):
    _proto_version = CLIENT_PROTO

    def __init__(self, context: zmq.Context, endpoint: ByteString, service: ByteString) -> None:
        self.context = context
        self.service = service
        self.endpoint = endpoint
        self.can_send = True
        self._proto_prefix = [b'', CLIENT_PROTO, service]
        self._tmo = None
        self.timed_out = False
        self.stream = None
        self._create_stream()

        _LOG.info(f"Initialized successfully with main endpoint [{endpoint.decode('utf-8')}]")

    def _create_stream(self) -> None:
        socket = self.context.socket(zmq.DEALER)
        ioloop = IOLoop.instance()
        self.stream = ZMQStream(socket, ioloop)
        self.stream.on_recv(self._on_message)
        self.stream.socket.setsockopt(zmq.LINGER, 0)
        self.stream.connect(self.endpoint)

    # TODO: Add this on the __del__ method to get automatically called
    def shutdown(self) -> None:
        if not self.stream:
            return
        _LOG.info("Shutting down all streams.")
        self.stream.close()
        self.stream = None

    def request(self, msg: MessageDataType, timeout: int = None) -> None:
        if not self.can_send:
            raise InvalidStateError()
        if isinstance(msg, bytes):
            msg = [msg]
        # prepare full message
        to_send = self._proto_prefix[:]
        to_send.extend(msg)
        if self.stream.closed():
            self._create_stream()
        self.stream.send_multipart(to_send)
        self.can_send = False
        if timeout:
            self._start_timeout(timeout)

    def _on_timeout(self) -> None:
        self.timed_out = True
        self._tmo = None
        self.on_timeout()

    def _start_timeout(self, timeout: int) -> None:
        self._tmo = IOLoop.current().call_later(timeout, self._on_timeout)

    def _on_message(self, msg: MessageDataType) -> None:
        if self._tmo:
            # disable timout
            self._tmo.stop()
            self._tmo = None
        # setting state before invoking on_message, so we can request from there
        self.can_send = True
        self.on_message(msg)

    def on_message(self, msg: MessageDataType) -> None:
        """Public method called when a message arrived.

        .. note:: Does nothing. Should be overloaded!
        """
        pass

    def on_timeout(self):
        """Public method called when a timeout occurred.

        .. note:: Does nothing. Should be overloaded!
        """
        pass


def mn_request(socket: zmq.Socket, service: ByteString, msg: MessageDataType, timeout: Optional[float] = None) \
        -> Optional[MessageDataType]:
    """Synchronous Guardian request.

    This function sends a request to the given service and
    waits for a reply.

    If timeout is set and no reply received in the given time
    the function will return `None`.
    """
    if not timeout or timeout < 0.0:
        timeout = None
    if isinstance(msg, bytes):
        msg = [msg]
    to_send = [CLIENT_PROTO, service]
    to_send.extend(msg)
    socket.send_multipart(to_send)
    ret = None
    rlist, _, _ = select([socket], [], [], timeout)
    if rlist and rlist[0] == socket:
        ret = socket.recv_multipart()
        ret.pop(0)  # remove service from reply
    return ret
